/*
 *License notice
 *
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement.
 *
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License.
 *
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied.
 *
 * See the License for the specific language governing permissions and limitations under the
 * License.
 *
 * All rights reserved.
 *
 */

/*global it, describe */
var assert = require("assert"),
    should = require("should"),
    avro = require("etp-avro"),
    etp = require('..');

var schemaCache = new etp.SchemaCache();

describe('EtpSchemaCache', function () {
    describe('#find()', function () {
        it("should find schemas by protocol and message type id", function () {
            var schema = schemaCache.find(0, 1);
            schema.name.should.equal("RequestSession");
        }.bind(this));
        it("should find error schemas irrespective of protocol", function () {
            var schema = schemaCache.find(2, 1000);
            schema.name.should.equal("ProtocolException");
        }.bind(this));
    });
});

function serializeDefaultClass(name)
{
    describe(name, function () {
        describe('#writeDatum()', function () {
            it("should write a " + name, function () {
                var datum = eval("new etp."+ name +"()");
                var writer = new avro.BinaryWriter(schemaCache);
                writer.writeDatum(name, datum);
            }.bind(this));
        });
    });

}

function serializeKnownValue(name, datum)
{
    describe(name, function () {
        describe('#writeDatum()', function () {
            it("should validate " + name + "from javascript hash.", function () {
                var schema = schemaCache[name];
                schemaCache.validate(schema, datum)
            }.bind(this));
        });
    });

}

serializeDefaultClass('Energistics.Datatypes.Version');
serializeKnownValue('Energistics.Datatypes.Version', {major: 1, minor: 2, revision: 3, patch: 4});
serializeDefaultClass('Energistics.Datatypes.ArrayOfDouble');
serializeKnownValue('Energistics.Datatypes.ArrayOfDouble', { values: [234, 2340, 12.3] } );
serializeDefaultClass('Energistics.Datatypes.DataValue');
serializeDefaultClass('Energistics.Datatypes.DataAttribute');
serializeDefaultClass('Energistics.Datatypes.MessageHeader');
serializeDefaultClass('Energistics.Datatypes.Version');
serializeDefaultClass('Energistics.Datatypes.SupportedProtocol');
serializeDefaultClass('Energistics.Datatypes.ChannelData.IndexMetadataRecord');
serializeDefaultClass('Energistics.Datatypes.ChannelData.ChannelMetadataRecord');
serializeDefaultClass('Energistics.Datatypes.ChannelData.DataItem');
serializeDefaultClass('Energistics.Datatypes.ChannelData.StreamingStartIndex');
serializeDefaultClass('Energistics.Datatypes.ChannelData.ChannelStreamingInfo');
serializeDefaultClass('Energistics.Datatypes.ChannelData.DataFrame');
serializeDefaultClass('Energistics.Datatypes.Object.Resource');
serializeDefaultClass('Energistics.Protocol.ChannelDataFrame.ChannelDataFrameSet');
serializeDefaultClass('Energistics.Protocol.ChannelDataFrame.RequestChannelData');
serializeDefaultClass('Energistics.Protocol.ChannelDataFrame.ChannelMetadata');
serializeDefaultClass('Energistics.Protocol.ChannelStreaming.ChannelData');
serializeDefaultClass('Energistics.Protocol.ChannelStreaming.ChannelRemove');
serializeDefaultClass('Energistics.Protocol.ChannelStreaming.ChannelDescribe');
serializeDefaultClass('Energistics.Protocol.ChannelStreaming.ChannelMetadata');
serializeDefaultClass('Energistics.Protocol.ChannelStreaming.ChannelRangeRequest');
serializeDefaultClass('Energistics.Protocol.ChannelStreaming.ChannelStatusChange');
serializeDefaultClass('Energistics.Protocol.ChannelStreaming.ChannelStreamingStart');
serializeDefaultClass('Energistics.Protocol.ChannelStreaming.ChannelStreamingStop');
serializeDefaultClass('Energistics.Protocol.ChannelStreaming.Start');
serializeDefaultClass('Energistics.Protocol.Core.Acknowledge');
serializeDefaultClass('Energistics.Protocol.Core.CloseSession');
serializeDefaultClass('Energistics.Protocol.Core.OpenSession');
serializeDefaultClass('Energistics.Protocol.Core.ProtocolException');
serializeDefaultClass('Energistics.Protocol.Core.RequestSession');
serializeDefaultClass('Energistics.Protocol.Store.PutObject');
serializeDefaultClass('Energistics.Protocol.Store.DeleteObject');
serializeDefaultClass('Energistics.Protocol.Store.GetObject');
serializeDefaultClass('Energistics.Protocol.Store.Object');
serializeDefaultClass('Energistics.Protocol.Discovery.GetResources');
serializeDefaultClass('Energistics.Protocol.Discovery.GetResourcesResponse');

//serializeDefaultClass('Energistics.Datatypes.ErrorCodes');
//serializeDefaultClass('Energistics.Datatypes.Protocols');
//serializeDefaultClass('Energistics.Datatypes.ChannelData.ChannelIndexTypes');
//serializeDefaultClass('Energistics.Datatypes.ChannelData.ChannelStatuses');
//serializeDefaultClass('Energistics.Datatypes.ChannelData.IndexDirections');
//serializeDefaultClass('Energistics.Datatypes.ChannelData.ErrorCodes');
//serializeDefaultClass('Energistics.Datatypes.ChannelData.IndexValue');
//serializeDefaultClass('Energistics.Datatypes.ChannelData.ChannelRangeInfo');
//serializeDefaultClass('Energistics.Datatypes.ChannelData.Roles');
//serializeDefaultClass('Energistics.Datatypes.Object.DataObject');
//serializeDefaultClass('Energistics.Protocol.ChannelStreaming.ChannelDataChange');

