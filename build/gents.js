/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

/*  gentd.js  - Typescript class from the ETP Avro schemas.
 *            - requires node.js
 *            - assumes the existence of .avpr file, which has been sorted 
 *  			into dependency order (see genProtocol.js, in the folder 
 *  			energyml/protocols/etpv1/build).
 *  
 *  Assumption is that there are no anonymous types except unions. In a pre-
 *  processing step, unions are decorated with unique types names and the
 *  fields containing the unions are converted to use this unique name. Unions
 *  still are of type array at base, and so are recognized as such and we 
 *  generate a custom class and traits for them.
 *  
 */

var fs = require('fs');

// Command-line arguments
var argv = require('optimist').default({
    protocolFile: './etp.avpr',
    outputFile: "./src/EtpMessages.ts"
}).argv;

if(argv.help) {
    require('optimist').showHelp();
    process.exit(0);
}

var references = [ /* "etp-avro/etp-avro.d.ts", "node/node.d.ts" */ ];
var tsImports = []; //[{name: "avro", module: "etp-avro"},{name:'foo',module:'./foo'}];
var jsRequires = [{name: 'Schemas', module: "./EtpSchemas.js"}];

var protocol = fs.readFileSync(argv.protocolFile, "ascii");
var schemas = JSON.parse(protocol).types;

var ClassMaker = function () {
};

ClassMaker.prototype = {
    buffer: "",
    indent: 0,
	unionCount: 0,
	anonymousUnions: [],
    definedTypes: {},
    messages:{},

    jsName: function(name) {
		switch(name) {
            case "null":
            case "int":
            case "long":
            case "float":
			case "double":
				return "number";
            case "string":
                return "string";
            case "boolean":
				return "boolean";
		}
        return name;
    },

// The next batch of methods are just helpers to create a pretty-printed output.	
	
    write: function (token) {
        for (var i = 0; i < this.indent; i++) {
            this.buffer += '\t';
        }
        this.buffer += token;
    },

    writeBlock: function (text) {
        var lines = text.split("\n");
        for (var line = 0; line < lines.length; line++) {
            this.write(lines[line]);
        }
    },

    start: function (token) {
        this.write(token);
        this.write("\n");
        this.indent++;
    },

    end: function (token) {
        this.indent--;
        this.write(token);
        this.write("\n");
    },

    beginNamespace: function (name) {
        var parts = name.split(".");
        parts.pop();
        for (var i = 0; i < parts.length; i++)
            this.start("export namespace " + parts[i] + " {");
    },

    endNamespace: function(name) {
        var parts = name.split(".");
        parts.pop();
        for (var i = 0; i < parts.length; i++)
            this.end("}");
    },
	
    line: function (token) {
        this.write(token + "\n");
    },

    typeOf: function (value) {
        var s = typeof value;
        if (s === 'object') {
            if (value) {
                if (value instanceof Array) {
                    s = 'array';
                }
                else {
                }
            } else {
                s = 'null';
            }
        }
        return s;
    },

	unqualifiedName: function(name) {
		return (""+name).split(".").pop();
	},
	
    writeUnion: function (name, types) {
		this.write("struct " + this.unqualifiedName(name) + " {\n");
		this.start("private:");
		this.write("size_t idx_;\n");
		this.write("boost::any value_;\n");
		this.end("");
		this.start("public:"); 
		this.write("size_t idx() const { return idx_; }\n");
        for (var i = 0; i < types.length; ++i) {
            var T = types[i];
            if (T == "null") {
				this.write("bool is_null() const { return idx_==" + i + "; }\n");
				this.write("void set_null() { idx_=" + i +"; value_ = boost::any(); }\n");
            }
            else if (typeof T == "object") {
                //constructors += "\n" + name + "(vector<" + this.jsName(T.type.items) + "> __object):__buffer(){ stringstream __sb; AvroEncode(__object, __sb); __buffer=__sb.str(); //m_position=" + i + ";}"
            }
            else {
				this.generateGetterAndSetter(this.jsName(T), this.unqualifiedName(T), i);
            }
        }
		//this.end("");
		this.end("};");
    },

	defaultValue: function(schema) {
        if (!schema)
            return "null";
        if(schema.default) {
            return schema.default;
        }
        switch (schema.type) {
			case "bytes":
            case "string":
                return "''";
            case "array":
                return "[]";
            case "boolean":
                return "false";
            case "int":
            case "long":
            case "float":
            case "double":
                return "0";
            case "enum":
                return schema.fullName + "." + schema.symbols[0];
            case "record":
                return "new " + schema.fullName + "()";
            default:
                return "null";
        }
	},

    writeType: function (schema, name) {
        var type;
        var i;
        var result;
        type = this.typeOf(schema);
        switch (type) {
            case "object":
                type = schema.type;
                break;
            case "string":
                type = schema;
                break;
            case "array":
                type = "union";
                break;
            default:
                throw "R:Invalid schema type: " + type + "in writeType()";
        }


        switch (type) {
            // Primitive types              
            case "null":
                return;
            case "boolean":
                this.line(name + ": boolean = " + this.defaultValue(schema) + ";");
                return;

            case "int":
            case "long":
            case "float":
            case "double":
                this.line(name + ": number = " + this.defaultValue(schema) + ";");
                return;

            case "bytes":
            case "string":
                this.line(name + ": string = " + this.defaultValue(schema) + ";");
                return;

                // Complex types
            case "record":
                if (this.definedTypes[name] === undefined) {
                    this.definedTypes[name] = schema;
                    this.beginNamespace(name);
                    this.start("export class " + name.split(".").pop() + "{");
                    for (i = 0; i < schema.fields.length; i++) {
                        this.writeType(schema.fields[i], ((schema.fields[i].type instanceof Array) ? schema.fields[i].name : schema.fields[i].name));
                    }
					this.line("static _schema : any = JSON.parse('" + JSON.stringify(schema) + "');");
					if(this.isMessage(schema)) {
						this.line("static _protocol : number = " + Number(schema.protocol) + ";");
						this.line("static _messageTypeId : number = " + Number(schema.messageType) + ";");
					}
                    this.end("}");
                    if (this.isMessage(schema)) {
                        this.line("export var Msg" + name.split(".").pop() + "=" + Number(schema.messageType) + ";");
                    }
                    this.endNamespace(name);
					
                }
                else {
                    this.line(name + " : " + name + ";");
                }


                return;

            case "enum": {
                console.log("Generating: " + name);
                this.definedTypes[name] = schema;
                var enumIdx;
                this.beginNamespace(name);
                this.start("export enum " + schema.name + " {");
                for (enumIdx = 0; enumIdx < schema.symbols.length; enumIdx++) {
                    this.line(schema.symbols[enumIdx] + "=" + enumIdx + ((enumIdx == (schema.symbols.length - 1)) ? "" : ","));
                }
                this.end("}");
                this.endNamespace(name);
            }
                return;

            case "array":
                this.line(name +":Array<" + this.jsName(schema.items) + ">=[];");
                return;

            case "map":
                this.line(name + ": any = {};");
                return result;

            case "union":
				/*
				console.log("union: " + schema.fullName);
                if (this.definedTypes[name] === undefined) {
                    this.definedTypes[name] = schema;
                    this.beginNamespace(name);
					this.writeUnion(name, schema);
                    this.endNamespace(name);
					
					var theName = this.jsName(schema.fullName);
					this.generateUnionTraits(theName, schema);
                }
                else {
                    this.line(name + " m_" + name + ";");
                }
				*/
                return;

            default:
                if (this.definedTypes[type] !== undefined) {
                    this.line(name + ":" + type + "= " + this.defaultValue(this.definedTypes[type]) + ";");
                }
                else {
                    this.writeType(schema.type, name);
                }
        }
    },
	
	isMessage: function(schema) {
		return ( schema.messageType >= 0 ) && ( schema.protocol >= 0 );
	},
	
    createClasses: function (schemas) {
		console.log("Processing schema list");
		
		for (var i = 0; i < schemas.length; i++) {
            console.log("Generating: " + schemas[i].fullName);			
            this.writeType(schemas[i], schemas[i].fullName);
        }
		this.definitions = this.buffer;		
    }
}


function isComplex(name) {
    return ['boolean', 'int', 'long', 'double', 'float', 'string', 'fixed', 'bytes', 'null', 'enum'].indexOf(name) < 0;
}

var classes = new ClassMaker();
classes.createClasses(schemas);

fdw = fs.openSync(argv.outputFile, 'w');
references.forEach(function(ref){
    fs.writeSync(fdw, "/// <reference path='" + ref + "'/>\n");

});

tsImports.forEach(function(imp){
    fs.writeSync(fdw, "export import " + imp.name + " = require('" + imp.module + "');\n");
});

jsRequires.forEach(function(req){
    fs.writeSync(fdw, "export var " + req.name + " = require('" + req.module + "');\n");
});

//fs.writeSync(fdw, "import avro=require('./Avro');\n\n");
fs.writeSync(fdw, classes.definitions);
fs.close(fdw);

console.dir(classes.messages);
