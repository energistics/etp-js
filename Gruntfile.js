module.exports = function(grunt) {

    grunt.initConfig({
        browserify: {
            dist: {
				options: {
					standalone: "etp"
				},
                files: {
                    "./lib/browser/etp.js": ["./lib/Etp.js"],
                    "./test/browser/tests.js": ["./test/*.js"]
                }
            }
        },
		execute: {
			gents: {
				src: ['build/gents.js']
			},
			genprot: {
				options: {
					args: ['./etp/build/genProtocol', '-p', '--outputProtocolFile=./etp.avpr', '--schemas=./etp/src/Schemas']
				},
				src: ['./etp/build/genProtocol.js']
			},
			genschema: {
				options: {
					args: ['./etp/build/genProtocol', '-s', '--outputJavaScriptFile=./lib/EtpSchemas.js', '--schemas=./etp/src/Schemas']
				},
				src: ['./etp/build/genProtocol.js']
			},
		},
		mochaTest: {
			default: {
				options: {
					
				},
				src: ['test/*.js']
			},
			xunit: {
				options: {
				  reporter: 'xunit',
				  captureFile: 'temp/xunit.xml'
				},
				src: ['test/*.js']
			  }
		},
        ts: {
            default : {
                tsconfig: "."
            }
        },
        typings: {
            install: {}
        },
        uglify: {
            options: {
                // the banner is inserted at the top of the output
                banner: '/*! bootstrap <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    './lib/browser/etp.min.js' : ['./lib/browser/etp.js']
                }
            }
        },
        watch: {
        }

    });


    grunt.loadNpmTasks("grunt-browserify")
    grunt.loadNpmTasks("grunt-contrib-uglify")
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-execute');
	grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks('grunt-typings');

    grunt.registerTask('init', ['typings']);
    grunt.registerTask('build', ['execute:genprot', 'execute:genschema', 'execute:gents', 'ts', 'browserify', 'uglify']);
	grunt.registerTask('test', ['mochaTest:default']);
	// xunit output for CI
	grunt.registerTask('xunit', ['mochaTest:xunit']);
	grunt.registerTask('all', ['init', 'build', 'test']);
	
    grunt.registerTask('default', ['build']);
};